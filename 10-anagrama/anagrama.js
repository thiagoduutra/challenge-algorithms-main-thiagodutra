function isAnagram(test, original) {
    //verificar se todas as strings são do mesmo tamanho
    if(test.length!==original.length){
        return false;
    }
    //toLowerCase para diminuir todas as letras
    //Split organizar as strings
    //Sort ordena os elementos
    //Join juntar
    var teste=test.toLowerCase().split('').sort().join('')
    var original2=original.toLowerCase().split('').sort().join('')
    return teste===original2
}
console.log(isAnagram("foefet", "toffee"), true) // true
console.log(isAnagram("Buckethead", "DeathCubeK"), true) // true
console.log(isAnagram("Twoo", "WooT"), true) // true

console.log(isAnagram("dumble", "bumble"), false) // false
console.log(isAnagram("ound", "round"), false) // false
console.log(isAnagram("apple", "pale"), false) // false
