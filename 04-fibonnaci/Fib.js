/*function fib(values) {
    let lista = []; //mostrando que a variável é um array
    lista[0] = 0; //primeiro número da sequência
    lista[1] = 1; //segundo número da sequência
     for(let i=2;i<values;i++){ //iniciando o i com 2, para pegar o o valor do segundo indice da sequência
        lista[i]= lista[i-1] + lista [i-2]; //fórmula
     }
     if(values===0){
        return 0
     }else if(values===1){
         return 1
     }
     return lista;
}*/

function fib(values){
   if(values < 2){ //valor menor que 2 é o valor dele mesmo
      return values
   }
   return fib(values-1) + fib(values-2) // maior que 2 da coloca a função
}

console.log(fib(0))
console.log(fib(1))
console.log(fib(2))
console.log(fib(3))
console.log(fib(4))
console.log(fib(5))
console.log(fib(35))
console.log(fib(46))