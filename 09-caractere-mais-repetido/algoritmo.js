function mostUsedChar(text) {
    let letraRep=0;
    for(let i=0;i<text.length;i++){
        let letra=text[i];
        let cont=0;
        for(let j=0; j<text.length;j++){
            if(letra==text[j]){
                cont++;
            }
        }
        if(cont>letraRep){
            letraRep=cont;
        }
    }
    return letraRep;
}

console.log(mostUsedChar("fdgdfgff"), 'f') // f
console.log(mostUsedChar("Lorem ipsum"), 'm') // m
console.log(mostUsedChar("adsassdasd"), 's') // s
console.log(mostUsedChar("testeeeee"), 'e') // e