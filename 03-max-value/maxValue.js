function maxValue(values){
    let maior = 0;
    for(let i=0; i<values.length;i++){
        if(values[i]>maior){
            maior=values[i];
        }
    }
    return maior;
}
console.log(maxValue([4,6,12,5]))
console.log(maxValue([9, 234, 312, 999, 21 , 2311]))
console.log(maxValue([533, 234, 23423, 32, 432]))
console.log(maxValue([5, 4, 3, 2, 1]))
