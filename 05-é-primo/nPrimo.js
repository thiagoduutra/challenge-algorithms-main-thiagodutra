function isPrime(number) {
    let contador=0;
    for(let i=1; i<=number;i++){
        if(number % i === 0){
            contador++
        }
    }
    if(contador === 2){ //se conseguiu dividir o número apenas duas vezes (ele mesmo e o um)
        return true
    }else{
        return false
    }
}

console.log(isPrime(2),"2") // true
console.log(isPrime(3),"3") // true
console.log(isPrime(4),"4") // false
console.log(isPrime(5),"5") // true
console.log(isPrime(6),"6") // false
console.log(isPrime(7),"7") // true
console.log(isPrime(8),"8") // false
console.log(isPrime(25),"25") // false
console.log(isPrime(31),"31") // true
console.log(isPrime(2017),"2017") // true
console.log(isPrime(2048),"2048") // false
console.log(isPrime(1),"1") // false